/// ETML
/// Author: Kevin Ferati
/// Date  : 18.05.2017
/// Description : Contains a static access to custom fonts

using Android.App;
using Android.Graphics;

namespace BGGStats
{
    public static class FontDistributor
    {
        public static Typeface DeltaJaeger = Typeface.CreateFromAsset(Application.Context.Assets, "Fonts/Delta Jaeger Book Regular.otf");
    }
}