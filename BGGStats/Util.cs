// ETML
/// Author   : Kevin Ferati
/// Date     : 17.05.2017
/// Description : Different generic usage static class
/// 

using Android.App;
using Android.Graphics;
using Java.Util;
using System.Net;
using System.Threading.Tasks;

namespace BGGStats
{
    public static class Util
    {
        /// <summary>
        /// Set a new locale to the application.
        /// </summary>
        /// <param name="locale">The new locale to set.</param>
        public static void SetLocale(Locale locale)
        {
            var res = Application.Context.Resources;
            var dm = res.DisplayMetrics;
            var conf = res.Configuration;
            conf.Locale = locale;

            res.UpdateConfiguration(conf, dm);
        }

        /// <summary>
        /// Return the number of px from dp
        /// </summary>
        public static int DpToPx(int dp)
        {
            var dm = Application.Context.Resources.DisplayMetrics;
            float scale = dm.Density;
            return (int)(dp * scale + 0.5f);
        }
        

        /// <summary>
        /// Return an image from an URL.
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public async static Task<Bitmap> GetBitmapFromURL(string url)
        {
            Bitmap img = null;
            try
            {
                WebClient client = new WebClient();
                // Read from the url, retrieve a stream and decode it into an image
                var stream = await client.OpenReadTaskAsync(url);
                img = await BitmapFactory.DecodeStreamAsync(stream);
            }
            catch
            {
                throw;
            }
            return img;
        }
    }
}