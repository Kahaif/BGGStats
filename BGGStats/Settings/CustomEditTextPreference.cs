/// ETML
/// Author : Kevin Ferati
/// Date : 18.05.2017
/// Description : Custom EditTextPReference that set the custom fonts and sizes

using Android.Content;
using Android.Preferences;
using Android.Util;
using Android.Views;

namespace BGGStats
{
    public class CustomEditTextPreference: EditTextPreference
    {
        public CustomEditTextPreference(Context context, IAttributeSet attrSet): base(context, attrSet){ }

        protected override void OnBindView(View view)
        {
            base.OnBindView(view);
            Summary = SharedPreferences.GetString(Key, Context.GetString(Resource.String.SettingsUsernameTitle));
        }
        
    }
}