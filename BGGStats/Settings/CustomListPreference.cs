/// ETML
/// Author : Kevin Ferati
/// Date : 18.05.2017
/// Description : Custom EditTextPReference that set the custom fonts and sizes

using Android.Content;
using Android.Preferences;
using Android.Util;
using Android.Views;

namespace BGGStats
{
    public class CustomListPreference: ListPreference
    {
        public CustomListPreference(Context context, IAttributeSet attrs): base(context, attrs) {}

        protected override void OnBindView(View view)
        {
            base.OnBindView(view);
            Summary = SharedPreferences.GetString(Key, Context.GetString(Resource.String.SettingsLanguagesTitle));
        }
    }
}