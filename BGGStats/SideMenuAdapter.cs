/// ETML
/// Author   : Kevin Ferati
/// Date     : 17.05.2017
/// Description : Manage how the side menu's element will be shown
/// 
using Android.Views;
using Android.Widget;
using Android.Content;

namespace BGGStats
{
    public class SideMenuAdapter : BaseAdapter
    {
        /// <summary>
        /// Elements to show
        /// </summary>
        string[] elements;

        /// <summary>
        /// This application context
        /// </summary>
        Context context;

        /// <summary>
        /// Hint that'll be used to know when to set a divider. Retrieved from resources.
        /// </summary>
        string divider;

        /// <summary>
        /// Number of element's to show.
        /// </summary>
        public override int Count => elements.Length;

        /// <summary>
        /// Default constructor. The elements are retrieved from resources' array.
        /// </summary>
        /// <param name="context">App's context.</param>
        public SideMenuAdapter(Context context)
        {
            elements = context.Resources.GetStringArray(Resource.Array.SideMenuElements);
            divider = context.Resources.GetString(Resource.String.SideMenuDividerElement);
            this.context = context;
        }
        
        /// <summary>
        /// Return the element from the specified position.
        /// </summary>
        /// <param name="position">Which position should </param>
        /// <returns></returns>
        public override Java.Lang.Object GetItem(int position)
        {
            if(position > -1 && position < elements.Length)
                return elements[position];

            return null;
        }

        /// <summary>
        /// Return the element that is at the given position's id.
        /// </summary>
        /// <param name="position">Element's position.</param>
        /// <returns>Item's ID.</returns>
        public override long GetItemId(int position)
        {
            return position;
        }

        /// <summary>
        /// Create the view of the specified position with the linked element.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="convertView">The inflated layout that'll be used to retrieve the views from the element layout and populate them with data</param>
        /// <param name="parent">View's parent.</param>
        /// <returns>An inflated layout</returns>
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            string element = elements[position];

            // Create the reusable view converter
            if(convertView == null)
            {
                LayoutInflater inflater = (LayoutInflater)context.GetSystemService(Context.LayoutInflaterService);
                convertView = inflater.Inflate(Resource.Layout.SideMenuElement, null);
            }
            
            // Retrieve the current element and append it the text
            var name = convertView.FindViewById<DeltaJaegerTextView>(Resource.Id.SideMenuElementContent);

            if (element == divider)
            {
                name.Background = context.GetDrawable(Resource.Drawable.SideMenuDivider);
                name.SetHeight(7);
                return convertView;
            }

            name.Text = element;
            name.Typeface = FontDistributor.DeltaJaeger;
            return convertView;
        }
    }
}