﻿/// ETML
/// Author   : Kevin Ferati
/// Date     : 12.05.2017
/// Description : Class defining a player in a game
/// 
using RestSharp.Deserializers;
namespace BGGStats.BGGApi
{
    public class Player
    {
        /// <summary>
        /// Player's username
        /// </summary>
        public string Username { get; set; }
        
        /// <summary>
        /// Player's id.
        /// </summary>
        [DeserializeAs(Name = "userid")]
        public int? Id { get; set; }

        /// <summary>
        /// Player's real name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Player's color during the play.
        /// </summary>
        public string Color { get; set; }

        /// <summary>
        /// Player's score during the play.
        /// </summary>
        public string Score { get; set; }

        /// <summary>
        /// Indicate whether the player is a new user of BGG. 
        /// </summary>
        public bool? New { get; set; }

        /// <summary>
        /// Indicate whether the player won.
        /// </summary>
        public bool? Win { get; set; }

        /// <summary>
        /// Player's rating of the play
        /// </summary>
        public string Rating { get; set; }
       
    }
}