/// ETML
/// Author   : Kevin Ferati
/// Date     : 17.05.2017
/// Description : Exception thrown if a request has been send but no correct user or object have been retrieved
/// 

using System;

namespace BGGStats.BGGApi
{
    public class InvalidUserObjectException : Exception
    {
        /// <summary>
        /// Indicates that the specified user or object is invalid or doesn't exist in BGG
        /// </summary>
        public InvalidUserObjectException() : base("The user or the object doesn't exist")
        {
           
        }
    }
}