/// ETML
/// Author   : Kevin Ferati
/// Date     : 12.05.2017
/// Description : Class defining a play
/// 
using RestSharp.Deserializers;
using System.Collections.Generic;

namespace BGGStats.BGGApi
{
    public class PlayWrapper
    {

        [DeserializeAs(Name = "total")]
        public int Total { get; set; }

        [DeserializeAs(Name = "page")]
        public int Page { get; set; }

        public List<Play> Plays { get; set; }
    }

    /// <summary>
    /// Define a play retrieved from /play.
    /// </summary>
    public class Play
    {
        /// <summary>
        /// Play's ID. 
        /// </summary>
        public int? Id { get; set; }

        /// <summary>
        /// When the game has been played
        /// </summary>
        public System.DateTime Date { get; set; }

        /// <summary>
        /// Where the play has been made. 
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// Play's comment. 
        /// </summary>
        public string Comments { get; set; }

        /// <summary>
        /// List of players that played during this play. 
        /// </summary>
        
        public List<Player> Players { get; set; }

        /// <summary>
        /// With which physical game the play has been made. 
        /// </summary>
        [DeserializeAs(Name = "item")]
        public PlayItem PlayedGame { get; set; }
    }
}