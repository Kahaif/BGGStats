/// ETML
/// Author   : Kevin Ferati
/// Date     : 12.05.2017
/// Description : Class that allows communication between the BGG server and C#
/// 
using Android.App;
using Android.Widget;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace BGGStats.BGGApi
{
    /// <summary>
    /// Rest client used to communicate with the BGG XML API
    /// </summary>
    public class BGGClient
    {
        
        /// <summary>
        /// BGG'S usefull routes.
        /// </summary>
        class Routes
        {
            public readonly static string THINGS = "/things";
            public readonly static string PLAYS = "/plays";
        }

        /// <summary>
        /// BGG's error that could be returned with an xml answer.
        /// </summary>
        class Errors
        {
            public readonly static string INVALID_USER_OBJECT = "Invalid object or user";
        }

        /// <summary>
        /// HTTP client that'll be used to send requests.
        /// </summary>
        RestClient client;
        readonly double maxPlaysPerPage = 100;

        /// <summary>
        /// BGG API's URL.
        /// </summary>
        public string BaseURL { get; private set; }

        /// <summary>
        /// Create a client that'll permit communication between this program and BGG'S XML API
        /// </summary>
        /// <param name="url">XML API BGG's url</param>
        public BGGClient(string url)
        {
            try
            {
                client = new RestClient(url);
            }
            catch
            {
                throw;
            }
            BaseURL = url;
        }

        /// <summary>
        /// Retrieve the plays that the specified user made.
        /// </summary>
        /// <param name="username">User which plays have to be retrieved. Must have at least one char.</param>
        /// <exception cref="Exception">Generic exception</exception>
        /// <exception cref="WebException">Thrown exception when this method couldn't communicate with BGG's API</exception>
        /// <exception cref="InvalidUserObjectException">Thrown exception when no item has been retrieved</exception>
        public async Task<PlayWrapper> GetPlaysAsync(string username, uint page = 1, string dateBegin = "", string dateEnd = "")
        {
            if (username.Length < 1 )
                throw new ArgumentException("must have at least one char", nameof(username));

            if (page < 1)
                throw new ArgumentException("can't be 0", nameof(page));

            var request = new RestRequest(Routes.PLAYS);
            request.AddQueryParameter("username", username);
            request.AddQueryParameter("page", page.ToString());

            if (dateBegin != "")
                request.AddQueryParameter("mindate", dateBegin);

            if (dateEnd != "")
                request.AddQueryParameter("maxdate", dateEnd);

            // Sending the request
            IRestResponse<PlayWrapper> response;

            try
            {
                response = await client.ExecuteGetTaskAsync<PlayWrapper>(request);
            }
            catch
            {
                throw;
            }

            //Checking for errors
            if (response.ErrorException != null)
                throw response.ErrorException;

            if (response.StatusCode != HttpStatusCode.OK)
                throw new WebException("Error while communicating with BGG API", WebExceptionStatus.UnknownError);

            if (response.Content.Contains(Errors.INVALID_USER_OBJECT))
                throw new InvalidUserObjectException();
            
            return response.Data;
        }

        /// <summary>
        /// Return every plays made by a user. Due to the slowness of BGG, it can take a lot of time.
        /// </summary>
        /// <param name="username">Which player to retrieve plays</param>
        /// <returns>An awaitable list of plays made by the given user. If no dates have been set, all plays are returned.</returns>
        public async Task<List<Play>> GetAllPlaysAsync(string username, string dateBegin = "", string dateEnd = "")
        {

            if (username.Length < 1)
                throw new ArgumentException("must have at least one char", username);

            // Require a first request to retrieve the number of page
            PlayWrapper initialPlays = null;
            try
            {
                initialPlays = await GetPlaysAsync(username, 1, dateBegin, dateEnd);
            }
            catch
            {
                throw;
            }

            var countPage = (uint) Math.Ceiling(initialPlays.Total / maxPlaysPerPage);

            

            var tasks = new List<Task<PlayWrapper>>();
            PlayWrapper[] completedTasks;

            // The first page has already been retrieved > start at two
            for (var i = 2; i < countPage + 1; i++)
            {
                tasks.Add(GetPlaysAsync(username, (uint) i, dateBegin, dateEnd));
            }

            var playsTasks = Task.WhenAll(tasks);

            try
            {
                completedTasks = await playsTasks;
                
            }
            catch
            {
                throw;
            }
            
            // Return a single list of plays tat also contains the initial plays
            var playsMade = new List<Play>();
            playsMade.AddRange(initialPlays.Plays);
            

            for (var i = 0; i < completedTasks.Length; i++)
            {
                playsMade.AddRange(completedTasks[i].Plays);
            }

            return playsMade;
        }


        /// <summary>
        /// Retrieve a thing (phyical object) from BGG
        /// </summary>
        /// <exception cref="Exception">Generic exception</exception>
        /// <exception cref="WebException">Thrown exception when this method couldn't communicate with BGG's API</exception>
        /// <exception cref="InvalidUserObjectException">Thrown exception when no item has been retrieved</exception>
        public async Task<ThingItem> GetThingAsync(int id, string thingType)
        {
            if (id < 1)
                throw new ArgumentOutOfRangeException(nameof(id), "Can't be smaller than 0");

            if (thingType.Length < 1)
                throw new ArgumentException(nameof(thingType), "Must have at least one char");

            var request = new RestRequest(Routes.THINGS);
            request.AddQueryParameter("id", id.ToString());
            request.AddQueryParameter("type", thingType);

            // Sending the request
            var response = await client.ExecuteGetTaskAsync<ThingItem>(request);

            if (response.ErrorException != null)
                throw response.ErrorException;

            if (response.StatusCode != HttpStatusCode.OK)
                throw new WebException();

            // If both of these properties are incorrect, the specified pair does not exist
            if (response.Data.Id == 0 && response.Data.ObjectType == null)
                throw new InvalidUserObjectException();

            return response.Data;
        }
    }
}