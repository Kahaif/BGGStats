/// ETML
/// Author   : Kevin Ferati
/// Date     : 12.05.2017
/// Description : Classes defining a physical item in BGG

using RestSharp.Deserializers;
using System.Collections.Generic;

namespace BGGStats.BGGApi
{
    /// <summary>
    /// Item retrieved from /plays. Contains basic information about an item.
    /// </summary>
    public class PlayItem
    {

        /// <summary>
        /// Item's name
        /// </summary>
        public string Name { get; set; }
        
        
        /// <summary>
        /// Item's Id
        /// </summary>
        [DeserializeAs(Name = "objectid")]
        public int Id { get; set; }


        /// <summary>
        /// Item's type
        /// </summary>
        public string ObjectType { get; set; }

        /// <summary>
        /// List of the item's subtype
        /// </summary>
        public List<SingleValue> Subtypes { get; set; }
        
    }
    

    /// <summary>
    /// Item retrieved from /thing. Contains more detailed information about an item.
    /// </summary>
    public class ThingItem: Java.Lang.Object
    {
        /// <summary>
        /// Item's Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Item's type
        /// </summary>
        [DeserializeAs(Name = "type")]
        public string ObjectType { get; set; }

        /// <summary>
        /// Item's name
        /// </summary>
        public Name Name { get; set; }

        /// <summary>
        /// URL to  this thing's image
        /// </summary>
        public string Image { get; set; }

        /// <summary>
        /// Url to this thing's thumbnail
        /// </summary>
        public string Thumbnail { get; set; }

       /// <summary>
       /// The year of the item's publication.
       /// </summary>
        public SingleValue YearPublished { get; set; }

        /// <summary>
        /// Min. number of players that can play this game.
        /// </summary>
        public SingleValue MinPlayers { get; set; }

        /// <summary>
        /// Max. Number of plays that can play this game.
        /// </summary>
        public SingleValue MaxPlayers { get; set; }

        /// <summary>
        /// Time duration in minute for a play of this game.
        /// </summary>
        public SingleValue PlayingTime { get; set; }

    }

    public class SingleValue
    {
        // !!! LET THIS NAME WITHOUT A CAPITAL FIRST LETTER OR IT WILL BUG
        public string value { get; set; }
    }

    /// <summary>
    /// Indicate an item's name
    /// </summary>
    public class Name
    {
        [DeserializeAs(Name = "value")]
        public string Value { get; set; }


        public int SortIndex { get; set; }

        [DeserializeAs(Name = "type")]
        public string NameType { get; set; }
    }
}