/// ETML
/// Author: Kevin Ferati
/// Date  : 31.05.2017
/// Description : Listener called by stats activity listening to the bar chart display the games
/// Every time a game is selected, display a dialog with the its information


using Android.Content;
using Android.Graphics;
using Android.Util;
using Android.Widget;
using BGGStats.BGGApi;
using Java.Lang;
using MikePhil.Charting.Charts;
using MikePhil.Charting.Data;
using MikePhil.Charting.Highlight;
using MikePhil.Charting.Listener;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BGGStats
{
    /// <summary>
    /// Display a dialog with an imageview and more information the game that has been selected.
    /// </summary>
    public class GameStatValueSelectedListener : Object, IOnChartValueSelectedListenerSupport
    {
        Context context;
        List<Play> plays;
        BGGClient client;
        BarChart chart;

        /// <summary>
        /// Cache for the games. The key is the game's id, the value is the game with the type ThingItem.
        /// </summary>
        LruCache gamesCache;

        /// <summary>
        /// Cache for the games' cover. The key is the game's id, the value is the game's cover as a Bitmap.
        /// </summary>        
        LruCache gamesCoverCache;

        /// <summary>
        /// Create a new instance of the value listener. Once a value is selected, it shows a dialog with that display a game and it's cover.
        /// </summary>
        /// <param name="context">Application context.</param>
        /// <param name="plays">List of plays that have been made.</param>
        /// <param name="client">BGG Client.</param>
        public GameStatValueSelectedListener(Context context, List<Play> plays, BGGClient client, BarChart chart)
        {
            this.context = context;
            this.plays = plays;
            this.client = client;
            this.chart = chart;

            int maxGamesCacheCount = 5;
            gamesCache = new LruCache(maxGamesCacheCount);
            gamesCoverCache = new LruCache(maxGamesCacheCount);
            
        }

        public void OnNothingSelected() { }
        

        public async void OnValueSelected(Entry e, Highlight h)
        {
            var entry = (BarEntry)e;
            var clickedGameName = (string)entry.Data;

            // Lookup for the clicked game from it's name
            var clickedGame = plays.Find(play => play.PlayedGame.Name == clickedGameName).PlayedGame;

            try
            {
                
                var playedGameThingItem = (ThingItem) gamesCache.Get(clickedGame.Id);
                var cover = (Bitmap)gamesCoverCache.Get(clickedGame.Id);

                Task<ThingItem> playedGameTask = null;
                Task<Bitmap> coverTask = null;

                // Start the task if the game was not cached
                if (playedGameThingItem == null)
                    playedGameTask = client.GetThingAsync(clickedGame.Id, clickedGame.Subtypes[0].value);

                var dialog = new GameDialog(context);
                dialog.Show();

                // If the request to retrieve the game has been made
                if(playedGameThingItem == null)
                {
                    playedGameThingItem = await playedGameTask;

                    // Cache the game
                    gamesCache.Put(playedGameThingItem.Id, playedGameThingItem);
                }

                dialog.DisplayGame(playedGameThingItem);


                // Retrieve the game cover and display it
                // Check if it's cached and start retrieving it if it's not
                if (cover == null)
                    coverTask = Util.GetBitmapFromURL(playedGameThingItem.Image);

                
                // If the image was not cached, retrieve and cache it
                if(cover == null)
                {
                    cover = await coverTask;
                    gamesCoverCache.Put(clickedGame.Id, cover);
                }

                dialog.DisplayGameCover(cover);

            }
            catch(Exception ex)
            {
                Toast.MakeText(context, Resource.String.ErrorRetrievingGame, ToastLength.Long).Show();
                Toast.MakeText(context, ex.Message, ToastLength.Long).Show();
            }
            
        }
    }
}