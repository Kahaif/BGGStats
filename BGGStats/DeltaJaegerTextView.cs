/// ETML
/// Author : Kevin Ferati
/// Date : 18.05.2017
/// Description : Custom TextView that implements a non-default font.

using Android.Content;
using Android.Util;
using Android.Widget;
namespace BGGStats
{
    public class DeltaJaegerTextView: TextView
    {
        /// <summary>
        /// Create a new TextView and applies it the DeltaJaeger font.
        /// </summary>
        /// <param name="context">App's Context</param>
        public DeltaJaegerTextView(Context context): base(context)
        {
            Typeface = FontDistributor.DeltaJaeger;
        }

        /// <summary>
        /// Create a new TextView, applies it the DeltaJaeger font and set the given text.
        /// </summary>
        /// <param name="context">Application's Context</param>
        /// <param name="text">Which text to set</param>
        public DeltaJaegerTextView(Context context, string text): this(context)
        {
            Text = text;
        }

        // Used by the XML Parser  => Do not remove ! 
        public DeltaJaegerTextView(Context context, IAttributeSet attrs) : base(context, attrs) { }
    }
}