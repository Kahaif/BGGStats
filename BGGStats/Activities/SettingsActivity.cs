﻿/// ETML
/// Author : Kevin Ferati
/// Date   : 18.05.2017
/// Description : Manage the settings
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Preferences;
using Android.Util;
using Android.Views;
using Android.Widget;
using Java.Util;
using System;
using System.Net.NetworkInformation;

namespace BGGStats
{
    [Activity(Label = "Settings", Theme = "@style/SettingsCategoriesHeader",
    ConfigurationChanges = ConfigChanges.Locale | ConfigChanges.Orientation
        | ConfigChanges.KeyboardHidden | ConfigChanges.ScreenSize, 
    ScreenOrientation = ScreenOrientation.SensorLandscape)]
    public class SettingsActivity : BaseActivity
    {

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            var smallUnitLayout = (LinearLayout)LayoutInflater.Inflate(Resource.Layout.SmallUnitLayout, null);

            // Adding the settings fragment
            // Restrain the fragment from creating itself multiple times 
            if (savedInstanceState == null)
            {
                var settingsFragment = new SettingsFragment();
                var transaction = FragmentManager.BeginTransaction();
                transaction.Add(Resource.Id.SmallUnitContent, settingsFragment);
                
                transaction.Commit();
            }
            
            contentLayout.AddView(smallUnitLayout);
        }
        
        /// <summary>
        /// Contains the settings components
        /// </summary>
        public class SettingsFragment: PreferenceFragment, Preference.IOnPreferenceChangeListener
        {
            public override void OnCreate(Bundle savedInstanceState)
            {
                base.OnCreate(savedInstanceState);
                AddPreferencesFromResource(Resource.Xml.Settings);
            }

            public bool OnPreferenceChange(Preference preference, Java.Lang.Object newValue)
            {
                var value = (string)newValue;

                // If the user changed the API url, ping the new url
                if (preference.Key == Application.Context.GetString(Resource.String.SettingsApiKey))
                {
                    try
                    {
                        var ping = new Ping();
                        var hostName = new Uri(value).Host;
                        var response = ping.Send(hostName);
                    }
                    catch
                    {
                        Toast.MakeText(Application.Context, GetString(Resource.String.SettingsApiFail), ToastLength.Long).Show();
                        return false;
                    }

                    Toast.MakeText(Application.Context, GetString(Resource.String.SettingsApiSuccess), ToastLength.Long).Show();
                    return true;
                }

                // Change the locale to the new value
                if (preference.Key == Application.Context.GetString(Resource.String.SettingsLanguagesKey))
                {
                    Util.SetLocale(new Locale(value));
                    RefreshActivity();
                }

                return true;
            }

            /// <summary>
            /// Refresh the parent activity
            /// </summary>
            protected void RefreshActivity()
            {
                Intent refresh = new Intent(Activity, typeof(SettingsActivity));
                refresh.AddFlags(ActivityFlags.ClearTop | ActivityFlags.NewTask);
                StartActivity(refresh);
            }

            public override void OnViewCreated(View view, Bundle savedInstanceState)
            {
                base.OnViewCreated(view, savedInstanceState);

                // Add the warning text
                var warningText = new DeltaJaegerTextView(Application.Context, GetString(Resource.String.SettingsWarning));
                warningText.SetTextSize(ComplexUnitType.Sp, 16);
                warningText.SetTextColor(Color.Red);
                warningText.SetPadding(4, 8, 4, 8);

                var layout = (LinearLayout)view;
                layout.AddView(warningText);

                FindPreference(Application.Context.GetString(Resource.String.SettingsApiKey)).OnPreferenceChangeListener = this;
                FindPreference(Application.Context.GetString(Resource.String.SettingsLanguagesKey)).OnPreferenceChangeListener = this;
            }
        }
    }
}