﻿/// ETML
/// Author: Kevin Ferati
/// Date : 17.05.2017
/// Description: Base activity for every activities. Manage the navigation drawer.

using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Preferences;
using Android.Support.V4.Widget;
using Android.Widget;
using Java.Lang;

namespace BGGStats
{
    public class BaseActivity : Activity
    {
        DrawerLayout mainLayout;
        ListView sideMenu;
        protected RelativeLayout contentLayout;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            

            // Retrieve the saved locale and applies it
            
            var currentLocale = PreferenceManager.GetDefaultSharedPreferences(this).GetString(GetString(Resource.String.SettingsLanguagesKey), "fr");
            Util.SetLocale(new Java.Util.Locale(currentLocale));
                        
            SetContentView(Resource.Layout.BaseLayout);
            
            mainLayout = FindViewById<DrawerLayout>(Resource.Id.MainLayout);
            contentLayout = FindViewById<RelativeLayout>(Resource.Id.Content);

            OverridePendingTransition(Resource.Animation.SlideIn, Resource.Animation.Hold);

            CreateSideMenu();
 
        }
        
        /// <summary>
        /// Create the lateral navigation menu and append it the navigation's events
        /// </summary>
        private void CreateSideMenu()
        {
            sideMenu = FindViewById<ListView>(Resource.Id.SideMenuList);
            sideMenu.Adapter = new SideMenuAdapter(this);

            // When an element is clicked, a new activity start and the user is sent to it
            sideMenu.ItemClick += (object sender, ListView.ItemClickEventArgs e) =>
            {
                mainLayout.CloseDrawer(sideMenu);
                var activityType = GetType();
               
                // Stats
                if (e.Position == 0 && activityType != typeof(StatsActivity))
                {
                    var goToStats = new Intent(this, typeof(StatsActivity));
                    StartActivity(goToStats);
                    return;
                }

                // Settings
                if(e.Position == 2 && activityType != typeof(SettingsActivity))
                {
                    var goToSettings = new Intent(this, typeof(SettingsActivity));
                    StartActivity(goToSettings);
                    return;
                }

                // Credits
                if(e.Position == 3 && activityType != typeof(CreditActivity))
                {
                    var goToCredit = new Intent(this, typeof(CreditActivity));
                    StartActivity(goToCredit);
                    return;
                }
            };
        }
        
    }
}

