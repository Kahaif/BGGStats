﻿/// ETML
/// Author: Kevin Ferati
/// Date  : 18.05.2017
/// Description : Main entry point of the application that'll also show the stats
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Preferences;
using Android.Text;
using Android.Util;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using BGGStats.BGGApi;
using Java.Util;
using MikePhil.Charting.Animation;
using MikePhil.Charting.Charts;
using MikePhil.Charting.Components;
using MikePhil.Charting.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BGGStats
{
    [Activity(Label = "Stats", MainLauncher = true,
    ScreenOrientation = ScreenOrientation.SensorLandscape,
    HardwareAccelerated = true,
    ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.KeyboardHidden | ConfigChanges.ScreenSize)]

    public class StatsActivity: BaseActivity
    {
        LinearLayout statsLayout;

        long chosenMonth = 0;
        int chosenYear = 2000;

        // Buttons
        Button clearUsername;
        
        Spinner  monthPicker;
        NumberPicker yearPicker;

        Button showVictoriesStatButton;
        Button showLocationsStatButton;
        Button showPlayersStatButton;
        Button showGamesStatButton;

        Button reloadButton;

        ProgressBar waitingForDataView;
        
        // Zones
        RelativeLayout zoneGraphical;
        LinearLayout zoneTextual;

        // Username input
        EditText username;

        BGGClient client;

        string currentUsername;
        string currentApiRoute;

        ImageView errorImage;

        bool dataRetrieved = false;

        // Stats' caches
        PieChart victoriesChart;
        DeltaJaegerTextView victoriesTextualStat;
        DeltaJaegerTextView loosesTextualStat;
        DeltaJaegerTextView totalTextualStat;

        BarChart locationsChart;
        List<DeltaJaegerTextView> locationsTextualStats;

        BarChart gamesChart;
        List<DeltaJaegerTextView> gamesTextualStats;

        BarChart playersChart;
        List<DeltaJaegerTextView> playersTextualStats;
        List<Play> plays;

        /// <summary>
        /// Maximum number of items displayed in the bar chart 
        /// </summary>
        const int X_VALUES_MAX = 8;
        const int X_VALUES_MIN = 1;
        
        const int X_LABEL_TEXT_SIZE_TO_REDUCE = 10;

        protected async override void OnCreate(Bundle savedInstanceState)
        {

            base.OnCreate(savedInstanceState);

            var sharedPref = PreferenceManager.GetDefaultSharedPreferences(this);
            currentUsername = sharedPref.GetString(GetString(Resource.String.SettingsUsernameKey), GetString(Resource.String.SettingsUsernameTitle));
            currentApiRoute = sharedPref.GetString(GetString(Resource.String.SettingsApiKey), GetString(Resource.String.SettingsApiDefaultValue));

            client = new BGGClient(currentApiRoute);

            statsLayout = (LinearLayout)LayoutInflater.Inflate(Resource.Layout.StatsLayout, null);

            // Zones
            zoneGraphical = statsLayout.FindViewById<RelativeLayout>(Resource.Id.StatsLayoutStatsGraphical);
            zoneTextual = statsLayout.FindViewById<LinearLayout>(Resource.Id.StatsLayoutStatsText);

            waitingForDataView = zoneGraphical.FindViewById<ProgressBar>(Resource.Id.LoadingData);
               

            int currentYear = Calendar.Instance.Get(CalendarField.Year);
            chosenYear = currentYear;

            RetrieveControls(currentYear);
            AppendControlEvents();
            errorImage = CreateErrorImage();
             
            SetButtonsEnabled(false);

            reloadButton.SetTextColor(Color.Black);

            var taskPlays = ReloadPlays(currentUsername, chosenMonth, chosenYear);

            contentLayout.AddView(statsLayout);

            // First load of plays
            try
            {
                plays = await taskPlays;
                

            }
            catch (InvalidUserObjectException)
            {
                Toast.MakeText(this, Resource.String.ErrorUserObjectDoesNotExist, ToastLength.Long).Show();
                zoneGraphical.AddView(errorImage);
            }
            catch
            {
                Toast.MakeText(this, Resource.String.ErrorCommunicationCheckNetwork, ToastLength.Long).Show();
                zoneGraphical.AddView(errorImage);
            }

            dataRetrieved = true;
            SetButtonsEnabled(true);
            
            ShowWinsLoosesStats(plays);
        }



        /// <summary>
        /// Enabled or disable  every buttons in the control zones.
        /// </summary>
        void SetButtonsEnabled(bool enabled)
        {
            reloadButton.Enabled = enabled;
            showGamesStatButton.Enabled = enabled;
            showLocationsStatButton.Enabled = enabled;
            showPlayersStatButton.Enabled = enabled;
            showVictoriesStatButton.Enabled = enabled;
        }

        /// <summary>
        ///  Create the error image and return it.
        /// </summary>
        ImageView CreateErrorImage()
        {
            var size = Util.DpToPx(48);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(zoneGraphical.LayoutParameters);
            layoutParams.AddRule(LayoutRules.CenterInParent);
            layoutParams.Width = size;
            layoutParams.Height = size;

            var img = new ImageView(this)
            {
                LayoutParameters = layoutParams
            };
            img.SetImageDrawable(GetDrawable(Resource.Drawable.ErrorImage));

            return img;
        }

        /// <summary>
        /// Refresh the plays from the server
        /// </summary>
        async Task<List<Play>> ReloadPlays(string username, long chosenMonth, int chosenYear)
        {
            // The dates have to be with the "YYYY-MM-DD" format
            var dateBegin = "";
            var dateEnd = "";

            // If "Whole year" has been chosen
            if(chosenMonth == 0)
            {
                dateBegin = chosenYear + "-01-01";
                dateEnd = chosenYear + "-12-31";
            }
            else
            {
                dateBegin = chosenYear + "-" + chosenMonth + "-01";
                dateEnd = chosenYear + "-" + chosenMonth + "-31";
            }

            List<Play> plays;
            try
            {
                plays = await client.GetAllPlaysAsync(username, dateBegin, dateEnd);
            }
            catch
            {
                throw;
            }

            return plays;
        }

        /// <summary>
        /// Append the Click event to the small units
        /// </summary>
        void AppendControlEvents()
        {
            var noPlays = GetString(Resource.String.StatsNoPlays);

            username.AfterTextChanged += (object sender, AfterTextChangedEventArgs e) =>
            {
                currentUsername = e.Editable.ToString();
                reloadButton.SetTextColor(Color.Red);
            };

            clearUsername.Click += (object sender, EventArgs e) =>
            {
                username.EditableText.Clear();

                var imm = (InputMethodManager) GetSystemService(Context.InputMethodService);
                imm.ToggleSoftInput(ShowFlags.Implicit, HideSoftInputFlags.ImplicitOnly);
            };


            monthPicker.ItemSelected += (object sender, AdapterView.ItemSelectedEventArgs e) =>
            {
                chosenMonth = e.Id;
                reloadButton.SetTextColor(Color.Red);
            };

            yearPicker.ValueChanged += (object sender, NumberPicker.ValueChangeEventArgs e) =>
            {
                chosenYear = e.NewVal;
                reloadButton.SetTextColor(Color.Red);
            };

            showVictoriesStatButton.Click += delegate
            {
                if (dataRetrieved)
                    ShowWinsLoosesStats(plays);
            };

            showLocationsStatButton.Click += delegate
            {
                if (dataRetrieved)
                    ShowLocationsStats(plays);
            };

            showGamesStatButton.Click += delegate
            {
                if (dataRetrieved)
                    ShowGamesStats(plays);
            };

            showPlayersStatButton.Click += delegate
            {
                if (dataRetrieved)
                    ShowPlayersCountStats(plays);
            };

            reloadButton.Click += async (object sender, EventArgs e) =>
            {

                if(currentUsername == "")
                {
                    Toast.MakeText(this, Resource.String.ErrorNoUsername, ToastLength.Short).Show();
                    return;
                }

                ClearStatsZones();
                waitingForDataView.Visibility = ViewStates.Visible;
                zoneGraphical.AddView(waitingForDataView);

                SetButtonsEnabled(false);

                dataRetrieved = false;

                Toast.MakeText(this, Resource.String.StatsStartRetrieve, ToastLength.Short).Show();

                plays = null;

                // Dispose every cached data
                victoriesChart = null;
                victoriesTextualStat = null;
                loosesTextualStat = null;

                locationsChart = null;
                locationsTextualStats = null;

                gamesChart = null;
                gamesTextualStats = null;

                playersChart = null;
                playersTextualStats = null;
                
                // Refresh the data

                try
                {
                    plays = await ReloadPlays(currentUsername, chosenMonth, chosenYear);
                }
                catch (InvalidUserObjectException)
                {
                    waitingForDataView.Visibility = ViewStates.Invisible;

                    Toast.MakeText(this, Resource.String.ErrorUserObjectDoesNotExist, ToastLength.Long).Show();
                    zoneGraphical.AddView(errorImage);
                }
                catch
                {
                    waitingForDataView.Visibility = ViewStates.Invisible;

                    Toast.MakeText(this, Resource.String.ErrorCommunicationCheckNetwork , ToastLength.Long).Show();
                    zoneGraphical.AddView(errorImage);
                }

                dataRetrieved = true;
                SetButtonsEnabled(true);
                
                ShowWinsLoosesStats(plays);
            };
        }

        /// <summary>
        /// Retrieve the buttons from main layout
        /// </summary>
        void RetrieveControls(int currentYear)
        {
            username = statsLayout.FindViewById<EditText>(Resource.Id.StatsControlUsername);
            username.ShowSoftInputOnFocus = true;
            username.Text = currentUsername;

            clearUsername = statsLayout.FindViewById<Button>(Resource.Id.StatsControlUsernameClear);
            monthPicker = statsLayout.FindViewById<Spinner>(Resource.Id.StatsControlDateMonth);
            yearPicker = statsLayout.FindViewById<NumberPicker>(Resource.Id.StatsControlDateYear);
            yearPicker.MinValue = 2000;
            yearPicker.MaxValue = currentYear;
            yearPicker.Value = currentYear;

            showVictoriesStatButton = statsLayout.FindViewById<Button>(Resource.Id.StatsControlVictories);
            showGamesStatButton = statsLayout.FindViewById<Button>(Resource.Id.StatsControlGame);
            showLocationsStatButton = statsLayout.FindViewById<Button>(Resource.Id.StatsControlLocations);
            showPlayersStatButton = statsLayout.FindViewById<Button>(Resource.Id.StatsControlPlayers);

            reloadButton = statsLayout.FindViewById<Button>(Resource.Id.StatsControlReload);
        }
        



        /// <summary>
        /// Triggered when the Victories buttons has been clicked.
        /// Calculate the number of victories and defeats if it hasn't been done before and display a pie chart with those informations.
        /// </summary>
        void ShowWinsLoosesStats(List<Play> plays)
        {
            ClearStatsZones();

            // If the stats have been cached
            if (victoriesChart != null)
            {
                zoneGraphical.AddView(victoriesChart);
                zoneTextual.AddView(victoriesTextualStat);
                zoneTextual.AddView(loosesTextualStat);
                zoneTextual.AddView(totalTextualStat);
                return;
            }

            if(plays == null || plays.Count == 0 )
            {
                zoneGraphical.AddView(CreateErrorMessage(GetString(Resource.String.StatsNoPlays)));
                return;
            }

            uint victories = 0;
            uint looses = 0;

            // Calculate the count of wins and looses if it hasn't been done before refreshing
            // Basically, iterate through the plays, check if there are players for the game
            // And check if the user has won the game
            foreach (Play play in plays)
            {
                if (play.Players == null || play.Players.Count == 0)
                    continue;

                foreach (Player player in play.Players)
                {
                    // Check if the current player is the one specified as the username
                    if (player.Username == currentUsername)
                    {
                        if (player.Win == true) victories++;
                        if (player.Win == false) looses++;
                    }
                }
            }
            
            // Display an error message
            if (victories == 0 && looses == 0)
            {
                zoneGraphical.AddView(CreateErrorMessage(GetString(Resource.String.StatsWinsLoosesNoData)));
                return;
            }

            var padding = Util.DpToPx(6);

            victoriesChart = CreateWinsLoosesStats(victories, looses);

            var txtStatParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WrapContent, LinearLayout.LayoutParams.WrapContent);

            victoriesTextualStat = new DeltaJaegerTextView(this, "Nombre de victoires : " + victories.ToString())
            {
                LayoutParameters = txtStatParams
            };

            victoriesTextualStat.SetPadding(0, padding, 0, padding); 

            loosesTextualStat = new DeltaJaegerTextView(this, "Nombre de défaites : " + looses.ToString())
            {
                LayoutParameters = txtStatParams
            };
            loosesTextualStat.SetPadding(0, padding, 0, padding);

            totalTextualStat = new DeltaJaegerTextView(this, "Total : " + (victories + looses).ToString())
            {
                LayoutParameters = txtStatParams
            };

            totalTextualStat.SetPadding(0, padding, padding, 0);

            zoneGraphical.AddView(victoriesChart);
            zoneTextual.AddView(victoriesTextualStat);
            zoneTextual.AddView(loosesTextualStat);
            zoneTextual.AddView(totalTextualStat);
        }

        /// <summary>
        /// Show the chart that displays the count of players
        /// </summary>
        /// <param name="plays"></param>
        void ShowPlayersCountStats(List<Play> plays)
        {
            ClearStatsZones();

            // Check if the player's chart has been cached before
            if(playersChart != null)
            {
                zoneGraphical.AddView(playersChart);
                playersTextualStats.ForEach(view => zoneTextual.AddView(view));
                return;
            }

            if (plays == null || plays.Count == 0)
            {
                zoneGraphical.AddView(CreateErrorMessage(GetString(Resource.String.StatsNoPlays)));
                return;
            }


            var playersCount = new Dictionary<string, int>();

            // Count the number of times a location appears in the plays
            foreach (Play play in plays)
            {
                if (play.Players == null || play.Players.Count == 0)
                    continue;

                var count = play.Players.Count.ToString();

                // If the pair already exists, increment the number of time the count of players appears
                if (playersCount.ContainsKey(count))
                {
                    playersCount[count]++;
                    continue;
                }
                // Else, create one
                playersCount.Add(count, 1);
            }
            

            // Display an error message if no location have been saved by the user
            if (playersCount.Count == 0)
            {
                zoneGraphical.AddView(CreateErrorMessage(GetString(Resource.String.StatsPlayersNoData)));
                return;
            }

            var sortedPlayersCount = playersCount.ToList().OrderByDescending(count => count.Value).ToList();
            var sortedPlayersCounttDictionary = sortedPlayersCount.ToDictionary(count => count.Key, count => count.Value);

            playersChart = CreatePlayersCountChart(sortedPlayersCounttDictionary);

            // Create the textual
            playersTextualStats = CreateTextualStats(sortedPlayersCounttDictionary);

            playersTextualStats.ForEach(view => zoneTextual.AddView(view));

            zoneGraphical.AddView(playersChart);
        }

        /// <summary>
        /// Show the number of times a game appears
        /// </summary>
        /// <param name="plays"></param>
        void ShowGamesStats(List<Play> plays)
        {
            ClearStatsZones();

            // Check if the chart has been cached before
            if(gamesChart != null)
            {
                zoneGraphical.AddView(gamesChart);
                gamesTextualStats.ForEach(view => zoneTextual.AddView(view));
                return;
            }

            if (plays == null || plays.Count == 0)
            {
                zoneGraphical.AddView(CreateErrorMessage(GetString(Resource.String.StatsNoPlays)));
                return;
            }
            
            // Count how many times a game appears
            var gamesCount = new Dictionary<string, int>();
            foreach (Play play in plays)
            {
                // As the game is always indicated, it's not required to check if it exists
                var name = play.PlayedGame.Name;

                // If the key already exists, increment the number of time the location appears
                if (gamesCount.ContainsKey(name))
                {
                    gamesCount[name]++;
                    continue;
                }
                // Else, create one
                gamesCount.Add(name, 1);
            }

            // Sort the Dictionary by the value
            var sortedGamesCount = gamesCount.ToList().OrderByDescending(game => game.Value).ToList();
            var sortGamesCountDictionary = sortedGamesCount.ToDictionary(game => game.Key, games => games.Value);

            gamesChart = CreateGamesChart(sortGamesCountDictionary, plays);
            
            // Creating the textual stats
            gamesTextualStats = CreateTextualStats(sortGamesCountDictionary);

            gamesTextualStats.ForEach(view => zoneTextual.AddView(view));

            zoneGraphical.AddView(gamesChart);
        }

        /// <summary>
        /// Show the chart displaying the number of plays per locations
        /// </summary>
        void ShowLocationsStats(List<Play> plays)
        {
            ClearStatsZones();

            // If the locations have been cached before
            if (locationsChart != null)
            {
                zoneGraphical.AddView(locationsChart);
                locationsTextualStats.ForEach(view => zoneTextual.AddView(view));
                return;
            }

            if (plays == null || plays.Count == 0)
            {
                zoneGraphical.AddView(CreateErrorMessage(GetString(Resource.String.StatsNoPlays)));
                return;
            }

            // The key is a location
            var locationCount = new Dictionary<string, int>();

            // Count the number of times a location appears in the plays
            foreach (Play play in plays)
            {
                if (play.Location == "")
                    continue;

                // If the key already exists, increment the number of time the location appears
                if (locationCount.ContainsKey(play.Location))
                {
                    locationCount[play.Location]++;
                    continue;
                }
                // Else, create one
                locationCount.Add(play.Location, 1);
            }

            // Display an error message if no location have been saved by the user
            if (locationCount.Count == 0)
            {
                zoneGraphical.AddView(CreateErrorMessage(GetString(Resource.String.StatsLocationsNoData)));
                return;
            }
            var sortedLocationCount = locationCount.ToList().OrderByDescending(location => location.Value).ToList();
            var sortedLocationCountDictionary = sortedLocationCount.ToDictionary(location => location.Key, location => location.Value);

           
            locationsChart = CreateLocationsChart(sortedLocationCountDictionary);
            zoneGraphical.AddView(locationsChart);
            
            locationsTextualStats = CreateTextualStats(sortedLocationCountDictionary);
            locationsTextualStats.ForEach(view => zoneTextual.AddView(view));

        }






        /// <summary>
        /// Return a fully formatted PieChart that display the victories and looses
        /// </summary>
        /// <param name="victories">Number of wins</param>
        /// <param name="looses">Number of looses</param>
        /// <returns>A formatted pie chart</returns>
        PieChart CreateWinsLoosesStats(uint victories, uint looses)
        {
            PieChart chart = new PieChart(this)
            {
                HighlightPerTapEnabled = false,
                CenterText = GetString(Resource.String.StatChartWinsLoosesCenterText)
                
            };
            chart.SetCenterTextSize(14);
            chart.SetCenterTextTypeface(FontDistributor.DeltaJaeger);
            chart.SetBackgroundColor(Color.White);
            chart.SetEntryLabelColor(Color.White);
            chart.AnimateY(1000, Easing.EasingOption.Linear);
            chart.Legend.Enabled = false;
            chart.Description.Enabled = false;

            var layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MatchParent, RelativeLayout.LayoutParams.MatchParent);
            layoutParams.AddRule(LayoutRules.CenterInParent);
            chart.LayoutParameters = layoutParams;


            var entries = new List<PieEntry>()
            {
                new PieEntry(victories, GetString(Resource.String.Wins)),
                new PieEntry(looses, GetString(Resource.String.Looses))
            };

            var pieDataSet = new PieDataSet(entries, "");
            pieDataSet.SetColors(Color.DarkBlue, Color.Red);

            PieData data = new PieData(pieDataSet);

            // Color and size of the small texts inside the pie
            data.SetValueTextSize(12);
            data.SetValueTextColor(Color.White);
            data.SetValueFormatter(new NoDecimalValueFormatter());

            // Finalizing the chart
            chart.Data = data;

            return chart;
        }

        /// <summary>
        /// Create a chart that displays the number of players per play
        /// </summary>
        /// <param name="playersCount"></param>
        /// <returns></returns>
        BarChart CreatePlayersCountChart(Dictionary<string, int> playersCount)
        {
            BarChart playerCountChart = CreateGenericBarChart();
            playerCountChart.XAxis.ValueFormatter = new StringArrayAxisValueFormatter(playersCount.Keys.ToArray());

            // Creating the entries
            var entries = new List<BarEntry>();
            int i = 0;
            foreach (KeyValuePair<string, int> countPair in playersCount)
            {
                entries.Add(new BarEntry(i, countPair.Value));
                i++;
            }

            playerCountChart.Data = new BarData(new BarDataSet(entries, "")
            {
                ValueFormatter = new NoDecimalValueFormatter(),
                Color = Color.DarkBlue,
                ValueTypeface = FontDistributor.DeltaJaeger
            });


            playerCountChart.SetVisibleXRangeMinimum(X_VALUES_MIN);
            playerCountChart.SetVisibleXRangeMaximum(X_VALUES_MAX);

            return playerCountChart;
        }

        /// <summary>
        /// Populate the generic bar with data and return it
        /// </summary>
        BarChart CreateGamesChart(Dictionary<string, int> gamesCount, List<Play> plays)
        {
            // Sort the games by the number of times they appear
            var gameChart = CreateGenericBarChart();

            gameChart.XAxis.ValueFormatter = new StringArrayAxisValueFormatter(gamesCount.Keys.ToArray());
            gameChart.XAxis.TextSize -= X_LABEL_TEXT_SIZE_TO_REDUCE;

            var entries = new List<BarEntry>();
            int i = 0;
            foreach (KeyValuePair<string, int> game in gamesCount)
            {
                var entry = new BarEntry(i, game.Value)
                {
                    Data = game.Key
                };
                
                entries.Add(entry);
                i++;
            }
                        
            var gameDataSet = new BarDataSet(entries, "")
            {
                ValueFormatter = new NoDecimalValueFormatter(),
                Color = Color.DarkBlue,
                ValueTypeface = FontDistributor.DeltaJaeger
            };


            gameChart.SetOnChartValueSelectedListener(new GameStatValueSelectedListener(this, plays, client, gameChart));

            gameChart.Data = new BarData(gameDataSet);
            
            gameChart.SetVisibleXRangeMinimum(X_VALUES_MIN);
            gameChart.SetVisibleXRangeMaximum(X_VALUES_MAX);
        

            return gameChart;
        }

        /// <summary>
        /// Populate the generic barchart with location's data and return it
        /// </summary>
        BarChart CreateLocationsChart(Dictionary<string, int> locationCount)
        {
            var locationChart = CreateGenericBarChart();

            locationChart.XAxis.ValueFormatter = new StringArrayAxisValueFormatter(locationCount.Keys.ToArray());
            locationChart.XAxis.TextSize -= X_LABEL_TEXT_SIZE_TO_REDUCE;
           
            var entries = new List<BarEntry>();

            int i = 0;
            foreach (KeyValuePair<string, int> location in locationCount)
            {
                entries.Add(new BarEntry(i, location.Value));
                i++;
            }

            var locationDataSet = new BarDataSet(entries, "")
            {
                ValueFormatter = new NoDecimalValueFormatter(),
                Color = Color.DarkBlue,
                ValueTypeface = FontDistributor.DeltaJaeger
            };

            locationChart.Data = new BarData(locationDataSet);

            locationChart.SetVisibleXRangeMaximum(X_VALUES_MAX);
            locationChart.SetVisibleXRangeMinimum(X_VALUES_MIN);
            
            return locationChart;
        }



        /// <summary>
        /// Create a generic bar chart that'll be used by Location, Games and players count which is conform to the analysis.
        /// Don't forget to populate it with data !
        /// </summary>
        /// <returns></returns>
        BarChart CreateGenericBarChart()
        {
            var chart = new BarChart(this);

            chart.SetBackgroundColor(Color.White);

            // Interactions
            chart.SetHighlightFullBarEnabled(false);
            chart.SetScaleEnabled(false);
            chart.HighlightPerDragEnabled = false;
            chart.DragEnabled = true;

            // Xaxis
            chart.XAxis.Position = XAxis.XAxisPosition.Bottom;
            chart.XAxis.GranularityEnabled = true;
            chart.XAxis.Granularity = 1f;
            chart.XAxis.SetDrawGridLines(false);

            // LeftAxis
            chart.AxisLeft.GranularityEnabled = true;
            chart.AxisLeft.Granularity = 1f;

            chart.AxisRight.Enabled = false;
            chart.Description.Enabled = false;
            chart.Legend.Enabled = false;

            chart.AnimateY(1000);
            
            chart.LayoutParameters = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MatchParent, RelativeLayout.LayoutParams.MatchParent);

            return chart;
        }
        
        /// <summary>
        /// Create the textual stats from the Dictionary.
        /// </summary>
        /// <returns>A list of formatted views with the format "key: value".</returns>
        List<DeltaJaegerTextView> CreateTextualStats(Dictionary<string, int> dataToShow)
        {
            List<DeltaJaegerTextView> views = new List<DeltaJaegerTextView>(dataToShow.Count);

            var layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WrapContent, LinearLayout.LayoutParams.WrapContent);
            var paddingTop = Util.DpToPx(6);

            foreach (KeyValuePair<string, int> pair in dataToShow)
            {
                var view = new DeltaJaegerTextView(this, pair.Key + " : " + pair.Value)
                {
                    LayoutParameters = layoutParams
                };
                view.SetTextSize(ComplexUnitType.Sp, 14);
                view.SetPadding(0, paddingTop, 0, paddingTop);

                views.Add(view);
            }

            return views;
        }
        
        /// <summary>
        /// Return an error text view
        /// </summary>
        /// <param name="error">Which error to display</param>
        DeltaJaegerTextView CreateErrorMessage(string error)
        {
            var errorTextView = new DeltaJaegerTextView(this, error);
            errorTextView.SetTextSize(ComplexUnitType.Sp, 26);
            return errorTextView;
        }
        

        /// <summary>
        /// Remove every view inside the stats zone
        /// </summary>
        void ClearStatsZones()
        {
            zoneGraphical.RemoveAllViews();
            zoneTextual.RemoveAllViews();
        }
    }
}