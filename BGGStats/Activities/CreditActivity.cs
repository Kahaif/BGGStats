﻿/// ETML
/// Author : Kevin Ferati
/// Date   : 18.05.2017
/// Description : Activity that'll display the credits
/// 
using Android.App;
using Android.OS;
namespace BGGStats
{
    [Activity(Label = "Credits")]
    public class CreditActivity: BaseActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }
    }
}