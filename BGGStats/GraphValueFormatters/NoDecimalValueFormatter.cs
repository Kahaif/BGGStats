/// ETML
/// Author: Kevin Ferati
/// Date : 17.05.2017
/// Description:
/// Value formatter used by MPAndroidChart to format little values displayed in the carts.
/// Remove the decimal from the value

using MikePhil.Charting.Formatter;
using MikePhil.Charting.Data;
using MikePhil.Charting.Util;


namespace BGGStats
{
    /// <summary>
    /// Format the given value to return a full digit without floating point number.
    /// </summary>
    public class NoDecimalValueFormatter : Java.Lang.Object, IValueFormatter
    {
        public string GetFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler)
        {
            return value.ToString();
        }
    }
}