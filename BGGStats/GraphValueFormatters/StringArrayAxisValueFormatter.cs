/// ETML
/// Author: Kevin Ferati
/// Date : 17.05.2017
/// Description:
/// Value formatter used by MPAndroidChart to format little values displayed outside of axises.
/// Display the value from a given string array.

using MikePhil.Charting.Components;
using MikePhil.Charting.Formatter;

namespace BGGStats
{
    public class StringArrayAxisValueFormatter : Java.Lang.Object, IAxisValueFormatter
    {
        string[] valuesToShow;

        /// <summary>
        /// Max number of chars displayed in the values.
        /// </summary>
        int maxLength = 15;

        /// <summary>
        /// Create a new formatter
        /// </summary>
        /// <param name="valuesToShow">Which values to substitute instead of numbers</param>
        public StringArrayAxisValueFormatter(string[] valuesToShow)
        {
            this.valuesToShow = valuesToShow;
        }

        public string GetFormattedValue(float value, AxisBase axis)
        {
            
            var formattedValue = valuesToShow[(int)value];

            // If the string is to big, trim the end
            if (formattedValue.Length > maxLength)
                formattedValue = formattedValue.Remove(maxLength) + "...";

            return formattedValue;
        }
    }
}