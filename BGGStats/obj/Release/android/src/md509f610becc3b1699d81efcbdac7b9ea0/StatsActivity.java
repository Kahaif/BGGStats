package md509f610becc3b1699d81efcbdac7b9ea0;


public class StatsActivity
	extends md509f610becc3b1699d81efcbdac7b9ea0.BaseActivity
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onCreate:(Landroid/os/Bundle;)V:GetOnCreate_Landroid_os_Bundle_Handler\n" +
			"";
		mono.android.Runtime.register ("BGGStats.StatsActivity, BGGStats, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", StatsActivity.class, __md_methods);
	}


	public StatsActivity () throws java.lang.Throwable
	{
		super ();
		if (getClass () == StatsActivity.class)
			mono.android.TypeManager.Activate ("BGGStats.StatsActivity, BGGStats, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onCreate (android.os.Bundle p0)
	{
		n_onCreate (p0);
	}

	private native void n_onCreate (android.os.Bundle p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
