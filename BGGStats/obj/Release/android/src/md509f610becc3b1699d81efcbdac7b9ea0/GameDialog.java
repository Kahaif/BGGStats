package md509f610becc3b1699d81efcbdac7b9ea0;


public class GameDialog
	extends android.app.Dialog
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onCreate:(Landroid/os/Bundle;)V:GetOnCreate_Landroid_os_Bundle_Handler\n" +
			"";
		mono.android.Runtime.register ("BGGStats.GameDialog, BGGStats, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", GameDialog.class, __md_methods);
	}


	public GameDialog (android.content.Context p0) throws java.lang.Throwable
	{
		super (p0);
		if (getClass () == GameDialog.class)
			mono.android.TypeManager.Activate ("BGGStats.GameDialog, BGGStats, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065", this, new java.lang.Object[] { p0 });
	}


	public void onCreate (android.os.Bundle p0)
	{
		n_onCreate (p0);
	}

	private native void n_onCreate (android.os.Bundle p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
