package md509f610becc3b1699d81efcbdac7b9ea0;


public class GameStatValueSelectedListener
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		com.github.mikephil.charting.listener.OnChartValueSelectedListener
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onNothingSelected:()V:GetOnNothingSelectedHandler:MikePhil.Charting.Listener.IOnChartValueSelectedListenerSupportInvoker, MPAndroidChart\n" +
			"n_onValueSelected:(Lcom/github/mikephil/charting/data/Entry;Lcom/github/mikephil/charting/highlight/Highlight;)V:GetOnValueSelected_Lcom_github_mikephil_charting_data_Entry_Lcom_github_mikephil_charting_highlight_Highlight_Handler:MikePhil.Charting.Listener.IOnChartValueSelectedListenerSupportInvoker, MPAndroidChart\n" +
			"";
		mono.android.Runtime.register ("BGGStats.GameStatValueSelectedListener, BGGStats, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", GameStatValueSelectedListener.class, __md_methods);
	}


	public GameStatValueSelectedListener () throws java.lang.Throwable
	{
		super ();
		if (getClass () == GameStatValueSelectedListener.class)
			mono.android.TypeManager.Activate ("BGGStats.GameStatValueSelectedListener, BGGStats, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onNothingSelected ()
	{
		n_onNothingSelected ();
	}

	private native void n_onNothingSelected ();


	public void onValueSelected (com.github.mikephil.charting.data.Entry p0, com.github.mikephil.charting.highlight.Highlight p1)
	{
		n_onValueSelected (p0, p1);
	}

	private native void n_onValueSelected (com.github.mikephil.charting.data.Entry p0, com.github.mikephil.charting.highlight.Highlight p1);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
