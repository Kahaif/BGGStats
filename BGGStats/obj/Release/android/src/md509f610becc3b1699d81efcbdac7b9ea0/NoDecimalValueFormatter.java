package md509f610becc3b1699d81efcbdac7b9ea0;


public class NoDecimalValueFormatter
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		com.github.mikephil.charting.formatter.IValueFormatter
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_getFormattedValue:(FLcom/github/mikephil/charting/data/Entry;ILcom/github/mikephil/charting/utils/ViewPortHandler;)Ljava/lang/String;:GetGetFormattedValue_FLcom_github_mikephil_charting_data_Entry_ILcom_github_mikephil_charting_utils_ViewPortHandler_Handler:MikePhil.Charting.Formatter.IValueFormatterInvoker, MPAndroidChart\n" +
			"";
		mono.android.Runtime.register ("BGGStats.NoDecimalValueFormatter, BGGStats, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", NoDecimalValueFormatter.class, __md_methods);
	}


	public NoDecimalValueFormatter () throws java.lang.Throwable
	{
		super ();
		if (getClass () == NoDecimalValueFormatter.class)
			mono.android.TypeManager.Activate ("BGGStats.NoDecimalValueFormatter, BGGStats, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public java.lang.String getFormattedValue (float p0, com.github.mikephil.charting.data.Entry p1, int p2, com.github.mikephil.charting.utils.ViewPortHandler p3)
	{
		return n_getFormattedValue (p0, p1, p2, p3);
	}

	private native java.lang.String n_getFormattedValue (float p0, com.github.mikephil.charting.data.Entry p1, int p2, com.github.mikephil.charting.utils.ViewPortHandler p3);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
