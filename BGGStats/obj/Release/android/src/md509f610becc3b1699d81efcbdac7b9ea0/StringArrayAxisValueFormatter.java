package md509f610becc3b1699d81efcbdac7b9ea0;


public class StringArrayAxisValueFormatter
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		com.github.mikephil.charting.formatter.IAxisValueFormatter
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_getFormattedValue:(FLcom/github/mikephil/charting/components/AxisBase;)Ljava/lang/String;:GetGetFormattedValue_FLcom_github_mikephil_charting_components_AxisBase_Handler:MikePhil.Charting.Formatter.IAxisValueFormatterInvoker, MPAndroidChart\n" +
			"";
		mono.android.Runtime.register ("BGGStats.StringArrayAxisValueFormatter, BGGStats, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", StringArrayAxisValueFormatter.class, __md_methods);
	}


	public StringArrayAxisValueFormatter () throws java.lang.Throwable
	{
		super ();
		if (getClass () == StringArrayAxisValueFormatter.class)
			mono.android.TypeManager.Activate ("BGGStats.StringArrayAxisValueFormatter, BGGStats, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}

	public StringArrayAxisValueFormatter (java.lang.String[] p0) throws java.lang.Throwable
	{
		super ();
		if (getClass () == StringArrayAxisValueFormatter.class)
			mono.android.TypeManager.Activate ("BGGStats.StringArrayAxisValueFormatter, BGGStats, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "System.String[], mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e", this, new java.lang.Object[] { p0 });
	}


	public java.lang.String getFormattedValue (float p0, com.github.mikephil.charting.components.AxisBase p1)
	{
		return n_getFormattedValue (p0, p1);
	}

	private native java.lang.String n_getFormattedValue (float p0, com.github.mikephil.charting.components.AxisBase p1);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
