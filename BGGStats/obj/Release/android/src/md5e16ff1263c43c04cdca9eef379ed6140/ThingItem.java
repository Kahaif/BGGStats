package md5e16ff1263c43c04cdca9eef379ed6140;


public class ThingItem
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("BGGStats.BGGApi.ThingItem, BGGStats, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", ThingItem.class, __md_methods);
	}


	public ThingItem () throws java.lang.Throwable
	{
		super ();
		if (getClass () == ThingItem.class)
			mono.android.TypeManager.Activate ("BGGStats.BGGApi.ThingItem, BGGStats, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
