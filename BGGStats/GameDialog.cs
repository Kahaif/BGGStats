/// ETML
/// Author: Kevin Ferati
/// Date  : 31.05.2017
/// Description : Dialog that'll show a game and it's cover. The layout used by this class is GameDialogLayout

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Views;
using Android.Widget;
using BGGStats.BGGApi;

namespace BGGStats
{

    public class GameDialog : Dialog
    {

        Context context;
        
        DeltaJaegerTextView nameView;
        DeltaJaegerTextView playingView;
        DeltaJaegerTextView minMaxPlayersView;
        DeltaJaegerTextView yearPublishedView;

        RelativeLayout imageContainer;
        ProgressBar waitingToLoad;
        ImageView coverView;
       
        /// <summary>
        /// Create a new game dialog
        /// </summary>
        /// <param name="context">Application's context</param>
        public GameDialog(Context context) : base(context)
        {
            this.context = context;
        }

        /// <summary>
        /// Display the basic information about a game, without it's cover
        /// </summary>
        /// <param name="gameToDisplay">Which game to display</param>
        public void DisplayGame(ThingItem gameToDisplay)
        {
            nameView.SetText(gameToDisplay.Name.Value, null);
            playingView.SetText(context.GetString(Resource.String.GameDialogPlayingTime) + " " + gameToDisplay.PlayingTime.value, null);
            minMaxPlayersView.SetText(context.GetString(Resource.String.GameDialogMinMaxPlayers) + " " + gameToDisplay.MinPlayers.value + " - " + gameToDisplay.MaxPlayers.value, null);
            yearPublishedView.SetText(gameToDisplay.YearPublished.value, null);
        }

        /// <summary>
        /// Display an image that represents the game, in top of the basic game's information 
        /// </summary>
        /// <param name="cover"></param>
        public void DisplayGameCover(Bitmap cover)
        {
            coverView.SetImageBitmap(cover);
            imageContainer.AddView(coverView);
            RemoveLoadView();
        }

        /// <summary>
        /// Remove the load progress bar from the dialog.
        /// </summary>
        public void RemoveLoadView()
        {
            waitingToLoad.Visibility = ViewStates.Gone;
        }

        /// <summary>
        /// Retrieve the game data and display them
        /// </summary>
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            Window.RequestFeature(WindowFeatures.NoTitle);
            SetContentView(Resource.Layout.GameDialogLayout);

            nameView = FindViewById<DeltaJaegerTextView>(Resource.Id.GameDialogName);
            playingView = FindViewById<DeltaJaegerTextView>(Resource.Id.GameDialogMinMaxPlayers);
            minMaxPlayersView = FindViewById<DeltaJaegerTextView>(Resource.Id.GameDialogYearPublished);
            yearPublishedView = FindViewById<DeltaJaegerTextView>(Resource.Id.GameDialogPlayingTime);
            waitingToLoad = FindViewById<ProgressBar>(Resource.Id.LoadingData);

            imageContainer = FindViewById<RelativeLayout>(Resource.Id.GameDialogImageContainer);
            imageContainer.Click += delegate
            {
                Dismiss();
            };

            // Settings cover attribute
            var imageParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MatchParent, RelativeLayout.LayoutParams.MatchParent);
            imageParams.AddRule(LayoutRules.CenterInParent, 1);

            coverView = new ImageView(context)
            {
                LayoutParameters = imageParams
            };
             
        }
    }
}